<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function  __construct()
    {
        parent::__construct();
		$this->load->model('admin_model');
    }

	public function index()
	{
		$this->load->view('admin');
	}

	public function promo_code_manager()
	{
		$this->load->view('promo_code_manager');
	}

	public function get_promocode_data()
	{
		$res = $this->admin_model->get_promocode_data();
		echo json_encode($res);
		exit;
	}

	public function generate_promocode()
	{
		$post = $this->input->post();
		$res = $this->admin_model->generate_promocode($post);
		echo json_encode($res);
		exit;
	}

	public function delete_promocode()
	{
		$id = $this->input->post('id');
		$res = $this->admin_model->delete_promocode($id);
		echo json_encode($res);
		exit;
	}
}
