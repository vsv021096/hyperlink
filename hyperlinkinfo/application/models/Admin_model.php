<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    public function get_promocode_data()
    {
        return $this->db->get('promocode_master')->result_array();
    }

    public function generate_promocode($post)
    {
        if ($post['promocode_count'] <= 0)
        {
            return array('status' => 'failed', 'message' => 'Enter Valid Count of promocode.');
        }
        if ($post['valid_from'] > $post['valid_till'])
        {
            return array('status' => 'failed', 'message' => 'Enter Valid Promocode valid date.');
        }
        $this->db->trans_start();
        for ($i = 0; $i < $post['promocode_count']; $i++)
        {
            $generated_code = $this->generate_code();
            $insert = array(
                'promocode_name' => strtoupper($generated_code),
                'discount' => $post['max_discount'],
                'valid_from' => $post['valid_from'],
                'valid_till' => $post['valid_till']
            );
            $res = $this->db->insert('promocode_master', $insert);
        }
        if ($res)
        {
            $this->db->trans_complete();
            return array('status' => 'success', 'message' => 'Promocode generated successfully.');
        }
        else
        {
            return array('status' => 'failed', 'message' => 'Problem in generating promocode. Try again later.');
        }
    }

    public function generate_code()
    {
        $type = ['MALE','FEMALE'];
        $code = $type[rand(0,1)].$this->generate_random_string();
        $this->db->select('promocode_master_id');
        $this->db->where('promocode_name', $code);
        $id = $this->db->get('promocode_master')->row_array()['promocode_master_id'];
        if ( ! empty($id))
        {
            $this->generate_code();
        }
        else
        {
            return $code;
        }
    }

    public function generate_random_string($length = 3) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function delete_promocode($id)
    {
        $sql = "SELECT applied_promocode_id
                FROM applied_promocode
                WHERE promo_id = ".$id;
        $res = $this->db->query($sql)->row_array()['applied_promocode_id'];
        if ( ! empty($res))
        {
            return array('status' => 'failed', 'message' => 'Promocode already user. It cant be deleted.');
        }
        else
        {
            $this->db->where('promocode_master_id', $id);
            if ($this->db->delete('promocode_master'))
            {
                return array('status' => 'success', 'message' => 'Promocode removed successfully.');
            }
            else
            {
                return array('status' => 'failed', 'message' => 'Problem in removing promocode. Try again later.');
            }
        }
    }
}
