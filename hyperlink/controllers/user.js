const User = require('../models/user');
const PromoCode = require('../models/promocode');

exports.getUserList = (req, res, next) => {
    User.fetchAll(result => {
        res.json(result);
    });
};

exports.postAddUser = (req, res, next) => {
    const name = req.body.name;
    const contact_no = req.body.contact_no;
    const email = req.body.email;
    const address = req.body.address;
    const gender = req.body.gender;
    const dob = req.body.dob;
    User.findByEmail(email, result => {
        if (res.length > 0){
            res.status(200).json({'status': 'failed', 'message':`${req.body.email} is already been registered`});
        }
        const userData = new User(null, name, contact_no, email, address, gender, dob);
        userData
            .save()
            .then(() => {
                res.status(201).json({'status': 'success', 'message':`User Registered Successfully.`});
            })
            .catch(err =>{
                console.log(err);
                res.status(401).json({'status': 'failed', 'message':`Problem in registration. Please try again later.`});
            });
    });
};

exports.applyPromotion = (req, res, next) => {
    const user_id = req.body.user_id;
    const promocode = req.body.promocode;
    PromoCode.checkPromocodeValid(promocode, result1 => {
        if (result.length > 0){
            const promo_id = result1.promocode_master_id;
            PromoCode.checkPromocodeAlreadyApplied((promocode, user_id), result => {
                if (result.length > 0){
                    res.status(200).json({'status': 'failed', 'message':`Promocode is already been used. Please apply another promocode.`});
                }
                const promotionData = new PromoCode(user_id, promo_id, null, null, null, null);
                promotionData
                    .applyPromotion()
                    .then(() => {
                        res.status(201).json({'status': 'success', 'message':`Promocode Applied Successfully.`});
                    })
                    .catch(err =>{
                        console.log(err);
                        res.status(401).json({'status': 'failed', 'message':`Problem in applying promocode. Please try again later.`});
                    });
            });
        } else {
            res.status(200).json({'status': 'failed', 'message':`Invalid promocode or promocode is expired.`});
        }
    });
};

