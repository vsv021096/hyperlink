const mysql = require('mysql2');

const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    database: 'hyperlink',
    password: ''
});

module.exports = pool.promise();