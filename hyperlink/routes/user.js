const express = require('express');

const userController = require('../controllers/user');

const router = express.Router();

// /user/get_user_list => GET
router.get('/get_user_list', userController.getUserList);

router.post('/add_user', userController.postAddUser);

router.post('/apply_promotion', userController.applyPromotion);

module.exports = router;
