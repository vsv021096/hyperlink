const db = require('../util/database');

module.exports = class User {
    constructor(user_id, name, contact_no, email, address, gender, dob) {
        this.user_id = user_id;
        this.name = name;
        this.contact_no = contact_no;
        this.email = email;
        this.address = address;
        this.gender = gender;
        this.dob = dob;
    }

    save() {
        return db.execute(
            'INSERT INTO user (name, contact_no, email, address, gender, dob) VALUES (?, ?, ?, ?, ?, ?)',
            [this.name, this.contact_no, this.email, this.address, this.gender, this.dob]
        );
    }

    static fetchAll() {
        return db.execute('SELECT * FROM user');
    }

    static findByEmail(email) {
        return db.execute('SELECT * FROM user WHERE email = ?', [email]);
    }

};
