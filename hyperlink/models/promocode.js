const db = require('../util/database');

module.exports = class Promocode {
    constructor(user_id, promocode_master_id, promocode_name, discount, valid_from, valid_till) {
        this.user_id = user_id;
        this.promocode_master_id = promocode_master_id;
        this.promocode_name = promocode_name;
        this.discount = discount;
        this.valid_from = valid_from;
        this.valid_till = valid_till;
    }

    static checkPromocodeValid() {
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        return db.execute(`SELECT * FROM promocode_master WHERE ${date} BETWEEN valid_from AND valid_till`);
    }

    static checkPromocodeAlreadyApplied(promocode_name, user_id) {
        return db.execute(`SELECT applied_promocode_id FROM applied_promocode ap INNER JOIN promocode_master pm ON pm.promocode_master_id = ap.promo_id WHERE user_id = ${user_id} AND promocode_name = ${promocode_name}`);
    }

    static addPromotion(email) {
        return db.execute('SELECT * FROM user WHERE email = ?', [email]);
    }

    applyPromotion(){
        return db.execute(
            'INSERT INTO applied_promocode (user_id, promo_id) VALUES (?, ?)',
            [this.user_id, this.promocode_master_id]
        );
    }

};
